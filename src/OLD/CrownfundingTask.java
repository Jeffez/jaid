package OLD;

import java.util.Calendar;

import com.google.gson.JsonElement;

public class CrownfundingTask implements ITask {

	public boolean daily;
	
	public CrownfundingTask(String hash) {
		
	}
	
	@Override
	public void run(Server server) {

		if(!params.containsKey("channel")) {
			System.err.println("Error - No channel_id");
		}
		Calendar rightNow = Calendar.getInstance();
		int hour = rightNow.get(Calendar.HOUR_OF_DAY);
		if (hour >= 0 && hour <= 1 && daily) {
			Application.sendMessage(
					new Message(handleTask()),
					Application.api.getTextChannelById(params.get("channel")),
					Application.servers.get(server.getId()).isPrettify());
			daily = false;
		}

		if (hour >= 20) {
			daily = true;
		}
		
		
	}

	private String handleTask() {

		StringBuilder result = new StringBuilder();
		result.append("Projet : ").append(params.get("name")).append("\n");
		JsonElement root;
		try {
			root = Parser.getElementForUrl(params.get("url"));
			if(params.get("url").contains("ulule")) {
				result.append(root.getAsJsonObject().get("amount_raised").getAsString());
			}
			if(params.get("url").contains("kickstarter")) {
				result.append(root.getAsJsonObject().getAsJsonObject("project").get("pledged").getAsString());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result.toString();
	}

}

package Application;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import Audio.MusicManager;
import Guild.Server;
import Guild.ServerBuilder;
import User.UserStat;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.EmbedType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageEmbed.AuthorInfo;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;

public class Application {

	public static ApplicationProperties		approp;
	public static JDA						api;
	public static MusicManager				musicManager;
	public static TradContent				tradContent;

	public static HashMap<String, Server>	servers				= new HashMap<String, Server>();
	public static HashMap<User, UserStat>	users				= new HashMap<User, UserStat>();

	public static String					updateTitle			= "";
	public static String					updateDescrition	= "";

	public static void main(String[] arguments) throws Exception {

		approp = new ApplicationProperties();

		api = JDABuilder.createDefault(approp.jdaToken)
				.enableIntents(GatewayIntent.MESSAGE_CONTENT,GatewayIntent.GUILD_PRESENCES, GatewayIntent.GUILD_MEMBERS )
				.setAutoReconnect(true)
				.setActivity(Activity.of(Activity.ActivityType.COMPETING, "v"+approp.jaidVersion))
				.addEventListeners(new ListenerDiscord())
				.build();

		initialization();
	}
	
	public static void initialization() {

		generateServers();

		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			Logs.error("Couldn't wait initialization");
		}

		musicManager = new MusicManager();
		try {
			tradContent = new TradContent();
		} catch (IOException e) {
			Logs.error("Couldn't load Trad");
		}

		// Get Jaid Update text
		try {
			List<String> update = Files.readAllLines(Paths.get("Files/jaid.update"), StandardCharsets.ISO_8859_1);
			for (String s : update) {
				if (s.startsWith("Mise � jour")) {
					updateTitle += s;
				} else {
					updateDescrition += s + "\n";
				}
			}
		} catch (Exception e1) {
			Logs.error(e1.toString());
		}

		servers.forEach((id, s) -> {
			try {
				if (approp.jaidUpdate) {
					Logs.info("Update and Starting");
					MessageEmbed embed = new MessageEmbed("", updateTitle, updateDescrition,
							EmbedType.RICH, null, 16744272, null, null, new AuthorInfo("J3ff3z", "", "http://kekkanbito.com/src/logo.jpg", ""), null,
							null, null, null);
					MessageCreateData message = new MessageCreateBuilder()
							.setEmbeds(embed)
							.build();
					// Channel app.prop
					api.getGuildById(id).getTextChannelsByName(approp.jaidGeneral, true).get(0).sendMessage(message).queue();

				} else {
					Logs.info("Starting");
				}
			} catch (Exception e) {
				Logs.error("Couldn't post update");
				System.err.println(e);
			}
		});

		// loadAllMusic();
		generateServerCommand();
	}
	
	public static void generateServers() {

		// Temporary Server Generation
		// Generate Server
		Server s = ServerBuilder.createDefault(approp.jaidServer)
				.setName("Djeff")
				.setLanguage("fr")
				.addCommand("music")
				.build();
		
		servers.put(s.getId(), s);
	}

	public static void generateServerCommand() {

		// Updating Commands list
		api.updateCommands().queue();

		servers.forEach((id, s) -> {
			s.cleanCommands(api);

			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e) {
				Logs.error("Couldn't wait initialization");
			}

			s.generateCommands(api);
		});

	}

	public static void loadAllMusic() {

		class LoadSilenceInAllGuild extends TimerTask {

			public void run() {

				Logs.info("Loading Silence sound in All Guild");
				for (Guild guild : api.getGuilds()) {

					VoiceChannel mainVocalChannel = guild.getVoiceChannels().get(0);
					guild.getAudioManager().openAudioConnection(mainVocalChannel);
					Logs.info("Connecting Audio to mainChannel");

					musicManager.loadTrack(guild, "https://www.youtube.com/watch?v=_VUKfrA9oLQ");
					musicManager.getPlayer(guild).getAudioPlayer().setFrameBufferDuration(5);
					Logs.info("Loading Track");
				}
			}
		}
		Timer timer = new Timer();
		timer.schedule(new LoadSilenceInAllGuild(), 0, 72000000);
	}
}

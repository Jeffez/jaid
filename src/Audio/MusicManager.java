package Audio;

import java.util.HashMap;
import java.util.Map;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import Application.Logs;
import net.dv8tion.jda.api.entities.Guild;

public class MusicManager {

	private final AudioPlayerManager		manager	= new DefaultAudioPlayerManager();
	private final Map<String, MusicPlayer>	players	= new HashMap<>();

	public MusicManager() {

		AudioSourceManagers.registerRemoteSources(manager);
		AudioSourceManagers.registerLocalSource(manager);
	}

	public synchronized MusicPlayer getPlayer(Guild guild) {

		if (!players.containsKey(guild.getId())) {
			AudioPlayer player = manager.createPlayer();
			players.put(guild.getId(), new MusicPlayer(player, guild));
		}
		return players.get(guild.getId());
	}

	public void loadTrack(final Guild guild, final String source) {

		MusicPlayer player = getPlayer(guild);

		guild.getAudioManager().setSendingHandler(player.getAudioHandler());

		manager.loadItemOrdered(player, source, new AudioLoadResultHandler() {

			@Override
			public void trackLoaded(AudioTrack track) {

				player.playTrack(track);
				Logs.info("Track Loaded");
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {

				Logs.info("Playlist Loaded");
			}

			@Override
			public void noMatches() {

				String message = "La piste [" + source + "] n'a pas �t� trouv�.";
				Logs.error(message);
			}

			@Override
			public void loadFailed(FriendlyException exception) {

				String message = "Impossible de jouer la piste (raison:" + exception.getMessage() + ")";
				Logs.error(message);

			}
		});

	}

}

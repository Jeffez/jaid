package OLD;

import java.util.HashMap;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

/**
 * return a String
 * 
 * @author Jeffez59
 *
 */
public class ClassicCmd implements ICommand {

	private String			NAME	= "raw";
	private final String	HELP	= "Usage !" + NAME;
	private String			message;

	public ClassicCmd(String name) {

		this.NAME = name;
	}

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {

		return true;
	}

	@Override
	public void action(String[] args, MessageReceivedEvent event) {

		HashMap<String, Server> servers = Application.servers;
		String id = event.getGuild().getId();
		System.out.println("[Classic Command] Id : " + id);
		if (servers.containsKey(id)) {
			try {
				HashMap<String, ClassicModule> map = Application.servers.get(id).getCommands();
				message = map.get(event.getMessage().getContentRaw().substring(1)).handle(args, event);
			} catch (NullPointerException e) {
				message = Application.getMessage("ClassicDefault", servers.get(id));
			}
			Application.sendMessage(
					new Message(edit(message, event)),
					event.getTextChannel(),
					false);
		}

	}

	@Override
	public String help() {

		return HELP;
	}

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {

		// System.out.println("[Cmd]" + String.valueOf(event.getAuthor().getName()) + "
		// use !" + NAME);
	}

}

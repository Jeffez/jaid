package OLD;

import java.util.regex.Pattern;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public interface ICommand {

	public boolean called(String[] var1, MessageReceivedEvent var2);

	public void action(String[] var1, MessageReceivedEvent var2);

	public String help();

	public void executed(boolean var1, MessageReceivedEvent var2);

	public default String edit(String message, MessageReceivedEvent event) {

		if (message == null) {
			return message;
		}

		if (message.contains("{User}")) {
			message = Pattern.compile("{User}", Pattern.LITERAL).matcher(message).replaceFirst(event.getAuthor().getName());
		}
		if (message.contains("{Channel}")) {
			message = Pattern.compile("{Channel}", Pattern.LITERAL).matcher(message).replaceFirst(event.getTextChannel().getName());
		}
		if (message.contains("{Server}")) {
			message = Pattern.compile("{Server}", Pattern.LITERAL).matcher(message).replaceFirst(event.getGuild().getName());
		}
		if (message.contains("{Content}")) {
			message = Pattern.compile("{Content}", Pattern.LITERAL).matcher(message).replaceFirst(event.getMessage().getContentRaw());
		}

		return message;
	}
}

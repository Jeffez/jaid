package OLD;

public class RSSItem {

	public String	title;
	public String	link;
	public String	description;

	/**
	 * An RSS Item
	 * 
	 * @param title
	 * @param link
	 * @param description
	 */
	public RSSItem(String title, String link, String description) {

		this.title = title;
		this.description = description;
		this.link = link;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			System.err.println("Pas de RSS");
			return false;
		}

		if (obj.getClass().equals(this.getClass())) {
			RSSItem rssi = (RSSItem) obj;
			if (rssi.title.equals(this.title) && rssi.description.equals(this.description) && rssi.link.equals(this.link)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public int hashCode() {

		return title.hashCode() + description.hashCode() + link.hashCode();
	}

}

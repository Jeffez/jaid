package OLD;

public class RSSUtils {

	public static Message createFromItem(RSSItem rssi) {

		Message message = new Message(rssi.description);

		message.title = rssi.title;
		message.url = rssi.link;

		return message;
	}

}

package OLD;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ListenerWeb
		extends Thread {

	private boolean	noobDaily	= true;
	private boolean	wakfuDaily	= true;

	public ListenerWeb(String string) {

		super(string);

		System.out.println("[Web Listener] Starting Thread");

	}

	@SuppressWarnings("static-access")
	@Override
	public void run() {

		while (true) {

			try {
				taskUpdate();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				System.out.println("[Web Listener] Connecting");
				JsonElement root = Parser.getElementForUrl(Application.approp.iwebUrl);
				System.out.println("[Web Listener] Reading JSON");
				JsonArray rootja = root.getAsJsonArray();

				for (JsonElement rootElement : rootja) {
					JsonObject rootobj = rootElement.getAsJsonObject();

					System.out.println("[Web Listener] Creating Server");
					String serverId = rootobj.getAsJsonObject().get("server_id").getAsString();
					System.out.println("[Web Listener] Server : " + serverId);
					String botName = rootobj.getAsJsonObject().get("bot_name").getAsString();
					boolean prettify = (rootobj.getAsJsonObject().get("classic_auth").getAsInt() == 1
							? true
							: false);

					if (!Application.servers.containsKey(serverId)) {
						Application.servers.put(serverId, new Server(serverId, botName, prettify, "fr"));
					} else {
						Application.servers.get(serverId).setBotName(botName);
						Application.servers.get(serverId).setPrettify(prettify);
					}

					System.out.println("[Web Listener] Creating Commands");
					JsonArray ja = rootobj.getAsJsonObject().getAsJsonArray("commands");
					for (JsonElement je : ja) {

						JsonObject jo = je.getAsJsonObject();

						String cmd = jo.get("cmd").getAsString();
						String cmdAnswer = jo.get("cmd_answer").getAsString();

						if (!Application.commands.containsKey(cmd)) {
							Application.commands.put(cmd, new ClassicCmd(cmd));
						}

						if (!Application.servers.get(serverId).getCommands().containsKey(cmd)) {
							Application.servers.get(serverId).getCommands().put(cmd, new ClassicModule(cmd,
									cmdAnswer));
						} else {
							Application.servers.get(serverId).getCommands().remove(cmd);
							Application.servers.get(serverId).getCommands().put(cmd, new ClassicModule(cmd,
									cmdAnswer));

						}
					}

					System.out.println("[Web Listener] Creating Modules");
					ja = rootobj.getAsJsonObject().getAsJsonArray("modules");

					for (JsonElement je : ja) {

						JsonObject jo = je.getAsJsonObject();

						String module = jo.get("module").getAsString();
						String hash = jo.get("hash").getAsString();

						if (!Application.modules.containsKey(module)) {
							Application.commands.put(module, new ModuleCmd(module));
						}

						if (!Application.servers.get(serverId).getModules().containsKey(module)) {
							Application.servers.get(serverId).getModules().put(module, moduleCreator(module,
									hash));

						} else {
							Application.servers.get(serverId).getModules().remove(module);
							Application.servers.get(serverId).getModules().put(module, moduleCreator(module,
									hash));
						}
						JsonArray params = jo.getAsJsonArray("params");

						for (JsonElement param : params) {

							JsonObject paramObject = param.getAsJsonObject();

							String paramString = paramObject.get("param").getAsString();
							String value = paramObject.get("value").getAsString();

							Application.servers.get(serverId).getModules().get(module).addParam(paramString, value,
									Application.servers.get(serverId));

						}

					}
					
					System.out.println("[Web Listener] Creating Task");
					ja = rootobj.getAsJsonObject().getAsJsonArray("tasks");
						
					for (JsonElement je : ja) {

						JsonObject jo = je.getAsJsonObject();

						String task = jo.get("task").getAsString();
						String hash = jo.get("hash").getAsString();

						if (!Application.servers.get(serverId).getTasks().containsKey(task)) {
							Application.servers.get(serverId).getTasks().put(task, taskCreator(task,
									hash));

						}
						JsonArray params = jo.getAsJsonArray("params");

						for (JsonElement param : params) {

							JsonObject paramObject = param.getAsJsonObject();

							String paramString = paramObject.get("param").getAsString();
							String value = paramObject.get("value").getAsString();

							Application.servers.get(serverId).getTasks().get(task).addParam(paramString, value,
									Application.servers.get(serverId));

						}

					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				TimeUnit.MILLISECONDS.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void taskUpdate() throws Exception {
		for (String s : Application.servers.keySet()) {
			Server server = Application.servers.get(s);
			for(String t : server.getTasks().keySet()) {
				server.getTasks().get(t).run(server);
			}
		}
	}

	private void RSSUpdate() {

		System.out.println("[Web Listener] RSS Update");
		try {
			for (String s : Application.servers.keySet()) {
				if (Application.servers.get(s).getModules().containsKey("rss")) {

					for (RSSFlux flux : ((RSSModule) Application.servers.get(s).getModules().get("rss")).fluxRss) {

						RSSItem rssi = Parser.getLastRss(flux.url);

						if (!rssi.equals(flux.lastItem)) {
							flux.lastItem = rssi;

							Application.sendMessage(RSSUtils.createFromItem(rssi), Application.api.getTextChannelById(flux.channel),
									Application.servers.get(s).isPrettify());
						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private IModule moduleCreator(String module, String hash) throws Exception {

		switch (module) {

			case "rule34":
				return new Rule34Module(hash);

			case "rss":
				return new RSSModule(hash);

			case "logs":
				return new LogsModule(hash);

			case "music":
				return new MusicModule(hash);

			case "math":
				return new MathModule(hash);
				
			case "emoji":
				return new EmojiModule(hash);

			default:
				System.out.println("[Web Listenner] Error, trying to add an non-existant module");

		}
		return null;
	}
	private ITask taskCreator(String task, String hash) throws Exception {

		switch (task) {

			case "crownfunding":
				return new CrownfundingTask(hash);
				
			default:
				System.out.println("[Web Listenner] Error, trying to add an non-existant task");

		}
		return null;
	}
}

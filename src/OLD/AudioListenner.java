package OLD;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;

public class AudioListenner extends AudioEventAdapter {

	private final BlockingQueue<AudioTrack>	tracks	= new LinkedBlockingQueue<>();
	private final MusicPlayer				player;

	public AudioListenner(MusicPlayer player) {

		this.player = player;

	}

	public BlockingQueue<AudioTrack> getTrack() {

		return tracks;
	}

	public int getTrackSize() {

		return tracks.size();
	}

	public void nextTrack() {

		if (tracks.isEmpty()) {
			player.getGuild().getAudioManager().closeAudioConnection();
		}
		player.getAudioPlayer().startTrack(tracks.poll(), false);
	}

	@Override
	public void onTrackStart(AudioPlayer player, AudioTrack track) {

		System.out.println("Starting " + track.getInfo().title);
	}

	@Override
	public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {

		System.out.println(endReason);
		if (endReason.mayStartNext) {
			nextTrack();
		}
	}

	public void queue(AudioTrack track) {

		if (!player.getAudioPlayer().startTrack(track, true)) {
			tracks.offer(track);
		}
	}

}

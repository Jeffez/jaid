package OLD;

public class RSSFlux {

	public String	channel;
	public String	url;
	public RSSItem	lastItem;

	public RSSFlux(String channel, String url) {

		this.channel = channel;
		this.url = url;

	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (obj.getClass().equals(this.getClass())) {
			RSSFlux rssf = (RSSFlux) obj;
			if (rssf.channel.equals(this.channel) && rssf.url.equals(this.url)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public int hashCode() {

		return channel.hashCode() + url.hashCode();
	}
}

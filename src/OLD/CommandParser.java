package OLD;

import java.util.*;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CommandParser {

	public CommandContainer parse(String rw, MessageReceivedEvent e) {

		String[] splitBeheaded;
		ArrayList<String> split = new ArrayList<String>();
		String raw = rw;
		String beheaded = raw.replace("!", "");
		String[] arrstring = splitBeheaded = beheaded.split(" ");
		int n = arrstring.length;
		int n2 = 0;
		while (n2 < n) {
			String s = arrstring[n2];
			split.add(s);
			++n2;
		}
		String invoke = (String) split.get(0);
		String[] args = new String[split.size() - 1];
		split.subList(1, split.size()).toArray(args);
		return new CommandContainer(raw, beheaded, splitBeheaded, invoke, args, e);
	}

	////////////////////////////////////////////
	/////////// Command Container //////////////
	////////////////////////////////////////////

	public class CommandContainer {

		public final String					raw;
		public final String					beheaded;
		public final String[]				splitBeheaded;
		public final String					invoke;
		public final String[]				args;
		public final MessageReceivedEvent	event;

		////////////////////////////////////////////

		public CommandContainer(String raw, String beheaded, String[] splitBeheaded, String invoke, String[] args, MessageReceivedEvent e) {

			this.raw = raw;
			this.beheaded = beheaded;
			this.splitBeheaded = splitBeheaded;
			this.invoke = invoke;
			this.args = args;
			this.event = e;
		}
	}
}

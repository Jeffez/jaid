package Application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import Commands.ICommand;
import Guild.Server;
import User.UserStat;
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

/**
 * @author Julien-Erwan
 *
 */
public class ListenerDiscord extends ListenerAdapter {

	@Override
	public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
		
		Server server =  Application.servers.get(event.getGuild().getId());
		
		ICommand c = server.getCommands().get(event.getName());
		
		event.reply(Application.tradContent.markupCheck(c.action(event), server)).setEphemeral(true).queue();
		
		c.executed(event);
		

		UserStat uStat ;
		
		if(Application.users.containsKey(event.getUser())) {
			uStat = Application.users.get(event.getUser());
		}else{
			Logs.info("Creating user");
			uStat = new UserStat(event.getUser());
			Application.users.put(event.getUser(), uStat);
		}
		
		uStat.addSlashUsed();
	}
	
	@Override
	public void onGuildMemberUpdateNickname(GuildMemberUpdateNicknameEvent event) {
		UserStat uStat ;
		
		if(Application.users.containsKey(event.getUser())) {
			uStat = Application.users.get(event.getUser());
		}else{
			Logs.info("Creating user");
			uStat = new UserStat(event.getUser());
			Application.users.put(event.getUser(), uStat);
		}
		
		uStat.addChangeName();
	}

	@Override
    public void onGuildVoiceUpdate( GuildVoiceUpdateEvent event) {

		UserStat uStat ;
		
		if(Application.users.containsKey(event.getMember().getUser())) {
			uStat = Application.users.get(event.getMember().getUser());
		}else{
			Logs.info("Creating user");
			uStat = new UserStat(event.getMember().getUser());
			Application.users.put(event.getMember().getUser(), uStat);
		}
		
		if(event.getChannelLeft() == null) {
			uStat.setVocalChannelTimeStart(System.currentTimeMillis());
		}
		
		if(event.getChannelJoined() == null) {
			uStat.addTimeInVocalChannel(System.currentTimeMillis());
		}
    }
	
	
	@Override
    public void onMessageReceived( MessageReceivedEvent event) {
		UserStat uStat ;
		
		if(Application.users.containsKey(event.getAuthor())) {
			uStat = Application.users.get(event.getAuthor());
		}else{
			Logs.info("Creating user");
			uStat = new UserStat(event.getAuthor());
			Application.users.put(event.getAuthor(), uStat);
		}
		
		uStat.addMessageCount();
		uStat.addMessageLength(event.getMessage().getContentDisplay().length());
		
		if(event.getMessage().getContentDisplay().equals("stat")) {
			Logs.info(uStat.toString());
		}
	}
	
	
	
	public void saveLog(MessageReceivedEvent event) throws IOException {

		File ff = new File("Logs/" + event.getGuild().getId() + ".log");
		ff.createNewFile();
		FileWriter ffw = new FileWriter(ff, true);
		ffw.write("\n[" + event.getMessage().getTimeCreated() + "] " + event.getChannel().getName() + " - " + event.getAuthor().getName() + " : "
				+ event.getMessage().getContentRaw() + "\n\r");
		ffw.close();
	}

}

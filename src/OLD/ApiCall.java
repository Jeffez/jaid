package OLD;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class ApiCall {

	public static String searchOnYoutube(String[] args) throws Exception {

		String url = Application.approp.youtubeSearchUrl;

		String search = "";

		for (int i = 1; i < args.length; i++) {
			search += args[i] + "%20";
		}

		url = url.replace("{search}", search);
		System.out.println(url);

		JsonElement root = Parser.getElementForUrl(url);

		return root.getAsJsonObject().get("items").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsJsonObject().get("videoId").getAsString();
	}

	public static String searchOnRule34(String query) throws Exception {

		JsonElement je = Parser.getJsonFromXmlForUrl(query);

		JsonArray posts = je.getAsJsonObject().get("posts").getAsJsonObject().get("post").getAsJsonArray();

		int size = posts.size();

		String imgUrl;

		int rand = (int) (Math.random() * size);

		imgUrl = posts.get(rand).getAsJsonObject().get("file_url").getAsString();

		return imgUrl;

	}

}

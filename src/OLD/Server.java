package OLD;

import java.util.HashMap;

public class Server {

	private String							id;
	private String							botName;
	private boolean							prettify;
	private String							lang;

	private HashMap<String, ClassicModule>	commands;
	private HashMap<String, IModule>		modules;
	private HashMap<String, ITask>			tasks;

	/**
	 * Server who contains all the commands and modules
	 * 
	 * @param id
	 * @param botName
	 * @param prettify
	 */
	public Server(String id, String botName, boolean prettify, String lang) {

		this.id = id;
		this.botName = botName;
		this.setPrettify(prettify);
		this.commands = new HashMap<String, ClassicModule>();
		this.modules = new HashMap<String, IModule>();
		this.tasks = new HashMap<String, ITask>();
		this.lang = lang;
	}

	/**
	 * 
	 * @return the id of the Server
	 */
	public String getId() {

		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(String id) {

		this.id = id;
	}

	/**
	 * 
	 * @return the name of the bot
	 */
	public String getBotName() {

		return botName;
	}

	/**
	 * 
	 * @param botName
	 */
	public void setBotName(String botName) {

		this.botName = botName;
	}

	/**
	 * 
	 * @return a map of the Commands
	 */
	public HashMap<String, ClassicModule> getCommands() {

		return commands;
	}

	/**
	 * 
	 * @param commands
	 */
	public void setCommands(HashMap<String, ClassicModule> commands) {

		this.commands = commands;
	}

	/**
	 * 
	 * @return a map of the modules
	 */
	public HashMap<String, IModule> getModules() {

		return modules;
	}

	/**
	 * 
	 * @param modules
	 */
	public void setModules(HashMap<String, IModule> modules) {

		this.modules = modules;
	}

	/**
	 * 
	 * @return a map of tasks
	 */
	public HashMap<String, ITask> getTasks() {

		return tasks;
	}

	/**
	 * 
	 * @param tasks
	 */
	public void setTasks(HashMap<String, ITask> tasks) {

		this.tasks = tasks;
	}

	/**
	 * 
	 * @return the lang of the Server
	 */
	public String getLang() {

		return lang;
	}

	/**
	 * 
	 * @param lang
	 */
	public void setLang(String lang) {

		this.lang = lang;
	}

	public boolean isPrettify() {

		return prettify;
	}

	public void setPrettify(boolean prettify) {

		this.prettify = prettify;
	}
}

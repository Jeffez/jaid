package OLD;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;
import org.json.XML;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Parser {

	public static JsonElement getElementForUrl(String urlString) throws Exception {

		URL url = new URL(urlString);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		JsonParser jp = new JsonParser();

		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		return root;
	}

	public static JsonElement getJsonFromXmlForUrl(String urlString) throws Exception {

		URL url = new URL(urlString);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		InputStream is = request.getInputStream();

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		String line = null;
		String xml = "";
		while ((line = reader.readLine()) != null) {
			xml += line;
		}
		reader.close();
		JSONObject xmlJSONObj = XML.toJSONObject(xml);
		JsonParser jp = new JsonParser();

		JsonElement root = jp.parse(xmlJSONObj.toString());
		return root;
	}

	public static RSSItem getLastRss(String flux) {

		try {
			JsonElement je = getJsonFromXmlForUrl(flux);

			JsonObject jo;
			RSSItem rssi = null;

			jo = je.getAsJsonObject().get("rss").getAsJsonObject().get("channel").getAsJsonObject().getAsJsonArray("item").get(0)
					.getAsJsonObject();
			rssi = new RSSItem(jo.get("title").getAsString(), jo.get("link").getAsString(), jo.get("description").getAsString());

			return rssi;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}

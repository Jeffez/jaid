package OLD;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import net.dv8tion.jda.api.entities.Guild;

public class MusicPlayer {

	private final AudioPlayer		audioPlayer;
	private final AudioListenner	listenner;
	private final Guild				guild;

	public MusicPlayer(AudioPlayer audioPlayer, Guild guild) {

		this.audioPlayer = audioPlayer;
		this.guild = guild;
		this.listenner = new AudioListenner(this);
		audioPlayer.addListener(this.listenner);

	}

	public AudioPlayer getAudioPlayer() {

		return audioPlayer;
	}

	public Guild getGuild() {

		return guild;
	}

	public AudioListenner getListenner() {

		return listenner;
	}

	public AudioHandler getAudioHandler() {

		return new AudioHandler(audioPlayer);
	}

	public synchronized void playTrack(AudioTrack track) {

		listenner.queue(track);
	}

	public synchronized void skipTrack() {

		listenner.nextTrack();
	}

}

package Application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Logs {

	public static void info(String data) {
		System.out.println("[INFO] : "+data);
		saveToLogsFile(data);
	}
	
	public static void error(String data) {
		System.out.println("[ERROR] : "+data);
		saveToErroFile(data);
		
	}
	
	public static void audio(String data) {
		System.out.println("[AUDIO] : " + data);
		saveToLogsFile(data);
	}
	
	public static void other(String data) {
		System.out.println("[OTHER] : "+data);
		saveToLogsFile(data);
	}
	
	public static void command(String data) {
		System.out.println("[CMD] : "+data);
		saveToLogsFile(data);
	}
	
	private static void saveToErroFile(String data) {
		
		File ff = new File("Logs/error.log");
		try {
			ff.createNewFile();
			FileWriter ffw = new FileWriter(ff, true);
			ffw.write("\n" + data +"\n\r");
			ffw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void saveToLogsFile(String data) {

		File ff = new File("Logs/info.log");
		try {
			ff.createNewFile();
			FileWriter ffw = new FileWriter(ff, true);
			ffw.write("\n" + data +"\n\r");
			ffw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

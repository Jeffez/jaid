package OLD;

import java.io.File;
import java.math.BigInteger;
import java.util.HashMap;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public interface IModule {
	
	public String					NAME	= "NULL";

	public String					request	= "";

	/**
	 * All the parameters of the Module
	 */
	public HashMap<String, String>	params	= new HashMap<String, String>();

	/**
	 * 
	 * @param args
	 * @param event
	 * @return the message to send for the event received
	 */
	public String handle(String[] args, MessageReceivedEvent event);

	/**
	 * 
	 * @param s
	 * @return the help message
	 */
	public String getHelp(Server s);

	/**
	 * Translate the hash to a Boolean Array
	 * 
	 * @param hash
	 * @return a boolean array
	 */
	public default Boolean[] translateHashToBoolArray(String hash) {

		String hashBit = new BigInteger(hash, 10).toString(2);
		Boolean[] array = new Boolean[hashBit.length()];
		for (int i = 0; i < hashBit.length(); i++) {
			if (hashBit.charAt(i) == '1') {
				array[i] = true;
			} else {
				array[i] = false;
			}
		}
		return array;
	}

	/**
	 * Add a param to the module
	 * 
	 * @param param
	 * @param value
	 * @param server
	 */
	public default void addParam(String param, String value, Server server) {

		if (param.equals("shortCmd")) {
			Application.commands.put(value, new ModuleCmd(value));
			server.getModules().put(value, this);
			params.put(param, value);
		} else {
			params.put(param, value);
		}
	}

	public Message getAnswer(Message messageObj, String request);
	
	public boolean hasFile();
	
	public File getFile();
}

package Application;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationProperties {

	public final Properties	prop	= new Properties();

	public String			jdaToken;
	public String			jdaGame;
	public String			iwebUrl;
	public String			rule34Url;
	public String			prettyfyColor;
	public String			youtubeSearchUrl;

	public boolean			jaidUpdate;
	public String			jaidVersion;
	public String			jaidServer;
	public String			jaidGeneral;
	public String			jaidBotName;
	public String			jaidLang;
	
	/**
	 * 
	 * @author Jeffez59
	 * @throws Exception
	 */
	public ApplicationProperties() throws Exception {

		InputStream input = new FileInputStream("Files/credentials.properties");

		prop.load(input);

		jdaToken = prop.getProperty("jda.token");
		iwebUrl = prop.getProperty("iweb.url");
		String googleApi = prop.getProperty("google.api");

		input.close();

		input = new FileInputStream("Files/constant.properties");

		prop.load(input);

		youtubeSearchUrl = prop.getProperty("youtube.searchapi") + googleApi;
		jdaGame = prop.getProperty("jda.game");
		rule34Url = prop.getProperty("rule34.link");
		prettyfyColor = prop.getProperty("prettify.color");
		
		jaidUpdate = prop.getProperty("jaid.update").equals("true");
		jaidVersion = prop.getProperty("jaid.version");
		jaidServer = prop.getProperty("jaid.server");
		jaidGeneral = prop.getProperty("jaid.general");
		jaidBotName = prop.getProperty("jaid.botName");
		jaidLang = prop.getProperty("jaid.lang");
		

		input.close();

	}

}

package Guild;

import java.util.HashMap;

import Application.Logs;
import Commands.ICommand;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.interactions.commands.Command;

public class Server {

	private String						id;
	private String						botName;
	private String						lang;

	private HashMap<String, ICommand>	commands;

	/**
	 * Server who contains all the commands and modules
	 * 
	 * @param id
	 * @param botName
	 * @param prettify
	 */
	public Server(String id, String botName, String lang) {

		this.id = id;
		this.botName = botName;
		this.commands = new HashMap<String, ICommand>();
		this.lang = lang;
	}

	/**
	 * 
	 * @return the id of the Server
	 */
	public String getId() {

		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(String id) {

		this.id = id;
	}

	/**
	 * 
	 * @return the name of the bot
	 */
	public String getBotName() {

		return botName;
	}

	/**
	 * 
	 * @param botName
	 */
	public void setBotName(String botName) {

		this.botName = botName;
	}

	/**
	 * 
	 * @return a map of the Commands
	 */
	public HashMap<String, ICommand> getCommands() {

		return commands;
	}

	/**
	 * 
	 * @return the lang of the Server
	 */
	public String getLang() {

		return lang;
	}

	/**
	 * 
	 * @param lang
	 */
	public void setLang(String lang) {

		this.lang = lang;
	}

	/**
	 * ******************** Method ********************
	 */

	/**
	 * 
	 * @param api
	 */
	public void cleanCommands(JDA api) {

		Logs.info("Cleaning commands");

		// For each Command existing in the Server, delete it
		api.getGuildById(id).retrieveCommands().queue(
				list -> {
					for (Command c : list) {
						api.deleteCommandById(c.getId()).queue();
					}
				});

		// Updating Commands list
		api.getGuildById(id).updateCommands().queue();
	}

	/**
	 * 
	 * @param api
	 */
	public void generateCommands(JDA api) {

		Logs.info("Adding commands");

		Guild g = api.getGuildById(id);

		commands.forEach((name, command) -> {
			g.upsertCommand(command.initialize()).queue();
		});

	}
}

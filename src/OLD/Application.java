package OLD;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

import Application.ListenerDiscord;
import net.dv8tion.jda.api.*;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.TextChannel;

public class Application {

	public static final CommandParser		parser			= new CommandParser();
	public static HashMap<String, ICommand>	commands		= new HashMap<String, ICommand>();
	public static HashMap<String, IModule>	modules			= new HashMap<String, IModule>();
	public static HashMap<String, Server>	servers			= new HashMap<String, Server>();
	public static ApplicationProperties		approp;
	public static DefaultText				defText;
	public static JDA						api;
	public static MusicManager				musicManager	= new MusicManager();

	public static void main(String[] arguments) throws Exception {

		approp = new ApplicationProperties();
		defText = new DefaultText();

		ListenerWeb listWeb = new ListenerWeb("Listen");
		listWeb.start();

		api = JDABuilder.createDefault(approp.jdaToken)
				.setAutoReconnect(true)
				.setActivity(Activity.watching(approp.jdaGame))
				.build();
		api.addEventListener(new ListenerDiscord());
		
		commands.put("help", new HelpCmd("help"));
	}

	public static void handleCommand(CommandParser.CommandContainer cmd) {

		if (commands.containsKey(cmd.invoke)) {
			boolean safe = commands.get(cmd.invoke).called(cmd.args, cmd.event);
			if (safe) {
				commands.get(cmd.invoke).action(cmd.args, cmd.event);
				commands.get(cmd.invoke).executed(safe, cmd.event);
			} else {
				commands.get(cmd.invoke).executed(safe, cmd.event);
			}
		}
	}

	public static String getMessage(String message, Server s) {

		return s.getLang().equals("fr") ? Application.defText.frText.get(message) : Application.defText.enText.get(message);
	}

	public static void sendMessage(Message message, TextChannel channel, Boolean prettify) {

		if (prettify) {
			channel.sendMessage(message.toEmbed()).queue();
		} else {
			channel.sendMessage(message.toClassic()).queue();
		}
	}

	public static void sendFile(TextChannel channel,File file) {

		channel.sendFile(file).queue();
	}
	
	public static String getListCmds(String id) {

		String liste = "";

		LinkedList<String> commandList = new LinkedList<String>();

		commandList.addAll(servers.get(id).getCommands().keySet());
		commandList.addAll(servers.get(id).getModules().keySet());

		for (String command : commandList) {
			liste += "!" + command + "\n";
		}

		return liste;
	}
}

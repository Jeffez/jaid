package OLD;

import java.util.HashMap;

public interface ITask {

	public String					NAME	= "NULL";

	/**
	 * All the parameters of the Task
	 */
	public HashMap<String, String>	params	= new HashMap<String, String>();
	
	/**
	 * @param args
	 * @param guild
	 */
	public void run(Server server);


	/**
	 * Add a param to the task
	 * 
	 * @param param
	 * @param value
	 * @param server
	 */
	public default void addParam(String param, String value, Server server) {
		params.put(param, value);
	}
}

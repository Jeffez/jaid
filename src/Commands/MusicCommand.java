package Commands;

import Application.*;

import Audio.*;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

/**
 * @author Julien-Erwan
 *
 */
public class MusicCommand implements ICommand {

	@Override
	public CommandData initialize() {

		OptionData musicOption = new OptionData(OptionType.STRING, "option", "All Music Options")
				.addChoice("Next", "next")
				.addChoice("Clear", "clear")
				.addChoice("Pause", "pause")
				.addChoice("Play", "unpause")
				.addChoice("Boost Volume", "bvolume")
				.addChoice("Hinder Volume", "hvolume")
				.addChoice("Mute", "mute")
				.addChoice("Unmute", "unmute")
				.addChoice("Info", "info");

		CommandData cd = Commands.slash("music", "Let's play music")
				.addOption(OptionType.STRING, "url", "The url you want to play")
				.addOptions(musicOption);
		return cd;
	}

	@Override
	public String action(SlashCommandInteractionEvent event) {

		Logs.info("Analyzing command");

		// Test if User is in a Voice Channel
		try {
			Logs.info("Connecting Audio to User Channel");
			AudioChannel channel = event.getGuild().getMember(event.getUser()).getVoiceState().getChannel();
			event.getGuild().getAudioManager().openAudioConnection(channel);
		} catch (Exception e) {
			return "$$err.audio.userNotInChannel$$";
		}

		// If there is an URL, add this to music player
		try {
			event.getOption("url").getAsString();
			return playMusic(event);
		} catch (Exception e) {
		}

		// If there is an option that isn't an URL, will analyse it
		try {
			event.getOption("option").getAsString();
			return updateMusicPlayer(event);
		} catch (Exception e) {
		}

		// No options, return help
		return help(event);
	}

	/**
	 * Control Audio Player
	 * 
	 * @param event
	 * @return
	 */
	private String updateMusicPlayer(SlashCommandInteractionEvent event) {

		switch (event.getOption("option").getAsString()) {
			case "next":
				return MusicController.nextTrack(event);
			case "clear":
				return MusicController.clear(event);
			case "pause":
				return MusicController.stop(event);
			case "unpause":
				return MusicController.play(event);
			case "bvolume":
				return MusicController.increaseVolume(event);
			case "hvolume":
				return MusicController.decreaseVolume(event);
			case "mute":
				return MusicController.mute(event);
			case "unmute":
				return MusicController.unmute(event);
			case "info":
				return MusicController.info(event);
		}

		return "Option non available";
	}

	/**
	 * Add YT Url to the bot
	 * 
	 * @param event
	 * @return
	 */
	private String playMusic(SlashCommandInteractionEvent event) {

		String data = event.getOption("url").getAsString();

		if (checkURL(data)) {
			return MusicController.addTrack(event);
		}

		return "$$err.audio.notYTURL$$";
	}

	/**
	 * Check if Data is a YT URL
	 * 
	 * @param data
	 * @return
	 */
	private boolean checkURL(String data) {

		String pattern = "^(http(s)?:\\/\\/)?((w){3}.)?youtu(be|.be)?(\\.com)?\\/.+";
		if (!data.isEmpty() && data.matches(pattern)) {
			return true;
		}
		return false;
	}

	@Override
	public void executed(SlashCommandInteractionEvent event) {

		Logs.info(event.getUser().getName() + " used " + event.getName());

	}

	@Override
	public String help(SlashCommandInteractionEvent event) {

		// TODO Auto-generated method stub
		return "$$audio.help$$";
	}

}

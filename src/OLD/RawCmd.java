package OLD;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class RawCmd implements ICommand {

	private final String	NAME	= "help";
	private final String	HELP	= "Usage !" + NAME;

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {

		return true;
	}

	@Override
	public void action(String[] args, MessageReceivedEvent event) {

	}

	@Override
	public String help() {

		return HELP;
	}

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {

		System.out.println("[Cmd]" + String.valueOf(event.getAuthor().getName()) + " use !" + NAME);
	}
}

package OLD;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class DefaultText {

	public final Properties			prop	= new Properties();

	public HashMap<String, String>	frText;
	public HashMap<String, String>	enText;

	public DefaultText() throws Exception {

		frText = new HashMap<String, String>();
		enText = new HashMap<String, String>();

		InputStream input = new FileInputStream("Files/lang.properties");

		prop.load(input);

		for (Object key : prop.keySet()) {
			String keyString = (String) key;
			if (keyString.startsWith("fr.")) {
				frText.put(keyString.substring(3), prop.getProperty(keyString));
			} else if (keyString.startsWith("en.")) {
				enText.put(keyString.substring(3), prop.getProperty(keyString));
			}
		}

	}
}

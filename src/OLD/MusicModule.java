package OLD;

import java.io.File;

import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class MusicModule implements IModule {

	public String NAME = "Music";

	public MusicModule(String hash) {

	}

	@Override
	public String handle(String[] args, MessageReceivedEvent event) {

		String message = "";
		VoiceChannel voiceChannel = event.getGuild().getMember(event.getAuthor()).getVoiceState().getChannel();

		if (voiceChannel == null) {
			message = "Not in Voice Channel\n";
			return message;
		}

		event.getGuild().getAudioManager().openAudioConnection(voiceChannel);

		if (args[0].startsWith("http")) {
			message = MusicController.addTrack(event);
		} else {
			switch (args[0]) {
				case "next":
					try {
						message = MusicController.nextTrack(event);
					} catch (NullPointerException e) {
						message = MusicController.clear(event);
					}
					break;
				case "clear":
					message = MusicController.clear(event);
					break;
				case "list":
					message = MusicController.list(event);
					break;
				case "stop":
					message = MusicController.stop(event);
					break;
				case "play":
					message = MusicController.play(event);
					break;
				case "louder":
					message = MusicController.increaseVolume(event);
					break;
				case "weaker":
					message = MusicController.decreaseVolume(event);
					break;
				case "volume":
					if (args.length <= 1) {
						message = MusicController.getVolume(event);
					} else {
						message = MusicController.setVolume(event, args[1]);
					}
					break;
				case "info":
					message = MusicController.getInfo(event);
					break;
				case "search":
					message = MusicController.search(event, args);
					break;
				default:
					message = "I don't know this command";
			}
		}

		// TODO Auto-generated method stub
		return message;
	}

	@Override
	public String getHelp(Server s) {

		return "This method can do :\n"
				+ "- next: Go to the next song in the queue"
				+ "- clear: Remove all music in the queue"
				+ "- list: List all the music in the queue"
				+ "- stop: Pause the actual track"
				+ "- play: Resume the actual track"
				+ "- louder: Increase Volume"
				+ "- weaker: Decrease Volume"
				+ "- volume: Return the actual volume or, set it to a new one"
				+ "- info: Return information on the actual track"
				+ "- search: Search and add to queue a track";
	}

	@Override
	public Message getAnswer(Message messageObj, String request) {

		String imgUrl = "";

		messageObj.title = "Module " + this.NAME;
		if (request.contains("info") || request.contains("search")) {
			imgUrl = messageObj.content.substring(messageObj.content.indexOf("{{") + 2, messageObj.content.indexOf("}}"));
			messageObj.content = messageObj.content.substring(0, messageObj.content.indexOf("{{"));
			messageObj.imageUrl = imgUrl;
		}

		return messageObj;
	}

	@Override
	public boolean hasFile() {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public File getFile() {

		// TODO Auto-generated method stub
		return null;
	}
}

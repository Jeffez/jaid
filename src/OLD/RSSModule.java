package OLD;

import java.io.File;
import java.util.ArrayList;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class RSSModule implements IModule {

	public String				NAME	= "RSS";

	public ArrayList<RSSFlux>	fluxRss	= new ArrayList<RSSFlux>();

	public RSSModule(String hash) {

		// Boolean[] arrayHashed = translateHashToBoolArray(hash);
	}

	@Override
	public String handle(String[] args, MessageReceivedEvent event) {

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getHelp(Server s) {

	
		return "WIP";
	}

	@Override
	public void addParam(String param, String value, Server server) {

		if (param.equals("flux")) {
			String[] values = value.split("%=%");

			RSSFlux rssf = new RSSFlux(values[0], values[1]);

			fluxRss.add(rssf);
		} else {
			params.put(param, value);
		}
	}

	public void addFlux(ArrayList<RSSFlux> RSSFluxSaved) {

		for (RSSFlux rssflux : RSSFluxSaved) {
			if (fluxRss.contains(rssflux)) {
				fluxRss.get(fluxRss.indexOf(rssflux)).lastItem = rssflux.lastItem;
			}
		}

	}

	@Override
	public Message getAnswer(Message messageObj, String request) {

		messageObj.title = "Module " + this.NAME;

		return messageObj;
	}

	@Override
	public boolean hasFile() {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public File getFile() {

		// TODO Auto-generated method stub
		return null;
	}
}

package OLD;

import java.io.File;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Rule34Module implements IModule {

	public String	NAME	= "Rule 34";

	// https://e621.net/
	public boolean	onlyNSFW;

	public Rule34Module(String hash) {

		Boolean[] arrayHashed = translateHashToBoolArray(hash);

		if (arrayHashed[0]) {
			onlyNSFW = true;
		} else {
			onlyNSFW = false;
		}

	}

	@Override
	public String handle(String[] args, MessageReceivedEvent event) throws NullPointerException {

		String message = "";

		if (onlyNSFW) {
			if (!event.getTextChannel().isNSFW()) {
				if (params.containsKey("rule34NSFW")) {
					return params.get("rule34NSFW");
				} else {
					return Application.getMessage("rule34NSFW", Application.servers.get(event.getGuild().getId()));
				}
			}
		}

		StringBuilder query = new StringBuilder();
		query.append(Application.approp.rule34Url + "&tags=");
		for (String s : args) {
			query.append(s);
			query.append("+");
		}
		if (query.toString().endsWith("+")) {
			query.deleteCharAt(query.length() - 1);
		}
		try {
			message = ApiCall.searchOnRule34(query.toString());
		} catch (Exception e) {
			if (params.containsKey("rule34NotFind")) {
				message = params.get("rule34NotFind");
			} else {
				message = Application.getMessage("rule34NotFind", Application.servers.get(event.getGuild().getId()));
			}
		}

		return message;
	}

	@Override
	public String getHelp(Server s) {

		return Application.getMessage("rule34Help", s);
	}

	@Override
	public Message getAnswer(Message messageObj, String request) {

		messageObj.title = "Module " + this.NAME;
		messageObj.imageUrl = messageObj.content;
		messageObj.url = messageObj.content;
		messageObj.content = request;

		return messageObj;
	}

	@Override
	public boolean hasFile() {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public File getFile() {

		// TODO Auto-generated method stub
		return null;
	}
}

package OLD;

import java.awt.Color;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;

public class Message {

	public String	content;
	public String	title;
	public String	url;
	public String	imageUrl;
	public String	thumbnailUrl;

	public Message(String content) {

		this.content = content;
	}

	public Message(String content, String title) {

		this.title = title;
		this.content = content;
	}

	public MessageEmbed toEmbed() {

		EmbedBuilder messagebuilder = new EmbedBuilder()
				.setColor(new Color(Integer.parseInt(Application.approp.prettyfyColor)));

		if (title != null) {
			messagebuilder.setTitle(title, url);
		}

		if (content != null) {
			messagebuilder.setDescription(content);
		}
		if (imageUrl != null) {
			messagebuilder.setImage(imageUrl);
		}

		if (thumbnailUrl != null) {
			messagebuilder.setThumbnail(thumbnailUrl);
		}

		// TODO Auto-generated method stub
		return messagebuilder.build();
	}

	public String toClassic() {

		// TODO Auto-generated method stub
		return content;
	}

}

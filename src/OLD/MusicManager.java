package OLD;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.sedmelluq.discord.lavaplayer.filter.equalizer.EqualizerFactory;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class MusicManager {

	private final AudioPlayerManager		manager	= new DefaultAudioPlayerManager();
	private final Map<String, MusicPlayer>	players	= new HashMap<>();

	public MusicManager() {

		AudioSourceManagers.registerRemoteSources(manager);
		AudioSourceManagers.registerLocalSource(manager);
	}

	public synchronized MusicPlayer getPlayer(Guild guild) {

		if (!players.containsKey(guild.getId())) {
			AudioPlayer player = manager.createPlayer();
			players.put(guild.getId(), new MusicPlayer(player, guild));
		}
		return players.get(guild.getId());
	}

	public void loadTrack(final MessageReceivedEvent event, final String source) {

		TextChannel channel = event.getTextChannel();

		MusicPlayer player = getPlayer(channel.getGuild());

		channel.getGuild().getAudioManager().setSendingHandler(player.getAudioHandler());

		//AudioListenner trackScheduler = new AudioListenner(player);

		
		manager.loadItemOrdered(player, source, new AudioLoadResultHandler() {

			@Override
			public void trackLoaded(AudioTrack track) {
				player.playTrack(track);
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {

				StringBuilder builder = new StringBuilder();
				int j = 0;
				for (int i = 0; i < playlist.getTracks().size(); i++) {
					AudioTrack track = playlist.getTracks().get(i);
					if (i <= 10) {
						builder.append(i).append(" **->** ").append(track.getInfo().title).append("\n");
					}
					player.playTrack(track);
					j = i;
				}
				builder.append("...").append(j - 10).append(" others musics");
				Message msg = new Message(edit(builder.toString(), event));
				msg.title = "Module Music";
				Application.sendMessage(msg, channel, Application.servers.get(channel.getGuild().getId()).isPrettify());
			}

			@Override
			public void noMatches() {

				String message = "La piste [" + source + "] n'a pas �t� trouv�.";
				Message msg = new Message(edit(message, event));
				msg.title = "Module Music";
				Application.sendMessage(msg, channel, Application.servers.get(channel.getGuild().getId()).isPrettify());
			}

			@Override
			public void loadFailed(FriendlyException exception) {

				String message = "Impossible de jouer la piste (raison:" + exception.getMessage() + ")";
				Message msg = new Message(edit(message, event));
				msg.title = "Module Music";
				Application.sendMessage(msg, channel, Application.servers.get(channel.getGuild().getId()).isPrettify());

			}
		});

	}

	public String edit(String message, MessageReceivedEvent event) {

		if (message.contains("{User}")) {
			message = Pattern.compile("{User}", Pattern.LITERAL).matcher(message).replaceFirst(event.getAuthor().getName());
		}
		if (message.contains("{Channel}")) {
			message = Pattern.compile("{Channel}", Pattern.LITERAL).matcher(message).replaceFirst(event.getTextChannel().getName());
		}
		if (message.contains("{Server}")) {
			message = Pattern.compile("{Server}", Pattern.LITERAL).matcher(message).replaceFirst(event.getGuild().getName());
		}
		if (message.contains("{Content}")) {
			message = Pattern.compile("{Content}", Pattern.LITERAL).matcher(message).replaceFirst(event.getMessage().getContentRaw());
		}

		return message;
	}
}

package Application;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Guild.Server;

public class TradContent {

	public final Properties			prop	= new Properties();

	public HashMap<String, String>	frText;
	public HashMap<String, String>	enText;

	public TradContent() throws IOException {

		frText = new HashMap<String, String>();
		enText = new HashMap<String, String>();

		InputStream input = new FileInputStream("Files/lang.properties");

		prop.load(input);

		for (Object key : prop.keySet()) {
			String keyString = (String) key;
			if (keyString.startsWith("fr.")) {
				frText.put(keyString.substring(3), prop.getProperty(keyString));
			} else if (keyString.startsWith("en.")) {
				enText.put(keyString.substring(3), prop.getProperty(keyString));
			}
		}
	}

	public String getTrad(String properties, Server s) {
		switch (s.getLang()) {
			case "fr":
				return frText.get(properties);
			default:
				return enText.get(properties);
		}
	}
	
	public String markupCheck(String string, Server server) {
				
		Pattern MY_PATTERN = Pattern.compile("\\$\\$(.*?)\\$\\$");
		Matcher m = MY_PATTERN.matcher(string);
		while (m.find()) {
		    String s = m.group(1);
		    string = string.replaceAll("\\$\\$"+s+"\\$\\$",this.getTrad(s,server));
		}
		
		return string;
	}

}

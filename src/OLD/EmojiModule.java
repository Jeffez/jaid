package OLD;

import java.io.File;
import java.io.IOException;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class EmojiModule implements IModule {

	public String	NAME	= "Emoji";	

	public EmojiModule(String hash) {

	}

	@Override
	public String handle(String[] args, MessageReceivedEvent event) throws NullPointerException {

		String message = "";

		int curseLevel = 1,x = 1 ,y = 1;
		
		
		if(args[0].equals("rand")) {
			try {
				curseLevel = Integer.parseInt(args[0]);
			}catch(Exception e) {
				
			}
	
			x = (int) Math.floor((Math.random() * 20 - 10)*curseLevel);
			y = (int) Math.floor((Math.random() * 20 - 10)*curseLevel);
			
		}else {
			x = Integer.parseInt(args[0]);
			y = Integer.parseInt(args[1]);
		}
		// Launch Python Command
		boolean isWindows = System.getProperty("os.name")
				  .toLowerCase().startsWith("windows");
		
		if (isWindows) {
			System.err.println("[Emoji Module] Error - Windows OS");
		} else {
		    try {
		    	Runtime.getRuntime().exec("rm temp.png");
				Process proc = Runtime.getRuntime()
				  .exec("python3 Executable/smiley_gen.py " + x + " " + y);
				proc.waitFor();
		    }
		    catch(IOException | InterruptedException e) {
				e.printStackTrace();
			}

		}
		return message;
	}

	@Override
	public String getHelp(Server s) {

		return Application.getMessage("rule34Help", s);
	}

	@Override
	public Message getAnswer(Message messageObj, String request) {

		messageObj.title = "Module " + this.NAME;
		messageObj.content = "...";

		return messageObj;
	}

	@Override
	public boolean hasFile() {

		return true;
	}

	@Override
	public File getFile() {
		
		return new File("temp.png");
		
	}
}

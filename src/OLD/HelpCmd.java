package OLD;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class HelpCmd implements ICommand {

	private String NAME = "raw";

	public HelpCmd(String name) {

		this.NAME = name;
	}

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {

		return true;
	}

	@Override
	public void action(String[] args, MessageReceivedEvent event) {

		String cmd = "";

		if (args.length == 0) {
			cmd = "help";
		} else {
			cmd = args[0];
		}
		Server s = Application.servers.get(event.getGuild().getId());

		String message = Application.getMessage("helpDefault", s);

		if (!cmd.equals("help")) {
			if (s.getCommands().containsKey(cmd)) {
				message = s.getCommands().get(cmd).getHelp(s);
			} else if (s.getModules().containsKey(cmd)) {
				message = s.getModules().get(cmd).getHelp(s);
			}
		} else {
			message = "Liste commandes :\n";
			message += Application.getListCmds(event.getGuild().getId());
		}
		Message messageObj = new Message(edit(message, event));
		Application.sendMessage(
				messageObj,
				event.getTextChannel(),
				Application.servers.get(event.getGuild().getId()).isPrettify());
	}

	@Override
	public String help() {

		return null;
	}

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {

		System.out.println("[Cmd]" + String.valueOf(event.getAuthor().getName()) + " use !" + NAME);
	}
}

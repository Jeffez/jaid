package User;

import net.dv8tion.jda.api.entities.User;

public class UserStat {
	
	private User user;

	// STAT
	private int		changeName;
	private int		messageCount;
	private int		slashUsed;

	private long	messageLength;
	private long	timeInVocalChannel; // in millisecond

	// TEMP STAT
	private long	vocalChannelTimeStart;

	public UserStat(User user) {
		this.user = user;
		
		this.changeName =0;
		this.messageCount = 0;
		this.slashUsed =0;
		this.messageLength =0;
		this.timeInVocalChannel = 0;
		this.vocalChannelTimeStart =0;
	}
	
	public int getChangeName() {

		return changeName;
	}

	public void addChangeName() {

		this.changeName++;
	}
	public int getMessageCount() {

		return messageCount;
	}

	public void addMessageCount() {

		this.messageCount++;
	}

	public int getSlashUsed() {

		return slashUsed;
	}

	public void addSlashUsed() {

		this.slashUsed++;
	}


	public long getTimeInVocalChannel() {

		return timeInVocalChannel;
	}

	public void addTimeInVocalChannel(long vocalChannelTimeStop) {

		this.timeInVocalChannel += (vocalChannelTimeStop - this.vocalChannelTimeStart);
	}

	public long getMessageLength() {

		return messageLength;
	}

	public void addMessageLength(long messageLength) {

		this.messageLength += messageLength;
	}

	public long getVocalChannelTimeStart() {

		return vocalChannelTimeStart;
	}

	public void setVocalChannelTimeStart(long vocalChannelTimeStart) {

		this.vocalChannelTimeStart = vocalChannelTimeStart;
	}

	@Override
	public String toString() {
		return this.user.getEffectiveName() + " : [" + this.changeName + "/" + this.messageCount + "/" + this.slashUsed + "/" + this.messageLength + "/" + this.timeInVocalChannel + "]" ;
	}

}

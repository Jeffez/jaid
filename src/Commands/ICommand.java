package Commands;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

public interface ICommand {

	public String action(SlashCommandInteractionEvent event);
	
	public CommandData initialize();

	public String help(SlashCommandInteractionEvent event);

	public void executed(SlashCommandInteractionEvent event);
}

package OLD;

import java.io.File;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class ClassicModule implements IModule {

	public String	NAME	= "";

	// private String cmd;
	private String	answer;

	/**
	 * 
	 * @param cmd
	 * @param answer
	 */
	public ClassicModule(String cmd, String answer) {

		// this.cmd = cmd;
		this.answer = answer;
	}

	@Override
	public String handle(String[] args, MessageReceivedEvent event) {

		return this.answer;
	}

	@Override
	public String getHelp(Server s) {

		return Application.getMessage("helpClassic", s) + this.answer;
	}

	@Override
	public Message getAnswer(Message messageObj, String request) {

		messageObj.title = "Module " + this.NAME;

		return messageObj;
	}

	@Override
	public boolean hasFile() {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public File getFile() {

		// TODO Auto-generated method stub
		return null;
	}

}

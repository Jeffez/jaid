package Guild;

import java.util.HashMap;

import Commands.*;
import Application.Application;

public class ServerBuilder {

	private String idServer;
	private String botName;
	private String lang;
	
	private HashMap<String, ICommand> commands = new HashMap<String, ICommand>();
	
	public ServerBuilder(String token) {
		this.idServer = token;
	}
	
    public static ServerBuilder createDefault(String token)
    {
        return new ServerBuilder(token).applyDefault();
    }
	
	private ServerBuilder applyDefault() {
		
		this.botName = Application.approp.jaidBotName;
		this.lang = Application.approp.jaidLang;
		
		return this;
	}

	public ServerBuilder setName(String name) {
		this.botName = name;
		return this;
	}
	
	public ServerBuilder setLanguage(String lang) {
		this.lang = lang;
		return this;
	}
	
	public ServerBuilder addCommand(String command) {
		switch (command){
			case "music" :
				this.commands.put("music", new MusicCommand());
				break;
			default :
				break;
		}
		return this;
	}
	
	public Server build() {
		
		Server server = new Server(idServer, botName, lang);
		server.getCommands().putAll(this.commands);
		
		return server;
	}
}

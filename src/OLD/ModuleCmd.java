package OLD;

import java.io.File;
import java.util.HashMap;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class ModuleCmd implements ICommand {

	private String			NAME;
	private final String	HELP	= "Usage !" + NAME;
	private String			message	= "";

	public ModuleCmd(String name) {

		this.NAME = name;
	}

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {

		return true;
	}

	@Override
	public void action(String[] args, MessageReceivedEvent event) {

		String id = event.getGuild().getId();
		if (Application.servers.containsKey(id)) {
			File file = null;
			Message messageObj;
			try {
				HashMap<String, IModule> map = Application.servers.get(id).getModules();
				message = map.get(this.NAME).handle(args, event);
				if (message == null) {
					return;
				}
				messageObj = new Message(edit(message, event));
				messageObj = map.get(this.NAME).getAnswer(messageObj, initialMessage(args));

				if(map.get(this.NAME).hasFile()) {
					file = map.get(this.NAME).getFile();
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
				message = Application.getMessage("ClassicDefault", Application.servers.get(id));
				
				messageObj = new Message(edit(message, event));
			}

			Application.sendMessage(
					messageObj,
					event.getTextChannel(),
					Application.servers.get(id).isPrettify());
			if(file != null) {
				Application.sendFile(event.getTextChannel(), file);
			}
		}
	}

	@Override
	public String help() {

		return HELP;
	}

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {

		System.out.println("[Cmd]" + String.valueOf(event.getAuthor().getName()) + " use !" + NAME);
	}

	public String initialMessage(String[] args) {

		String message = "";

		for (String arg : args) {
			message += arg + " ";
		}

		return message;
	}
}

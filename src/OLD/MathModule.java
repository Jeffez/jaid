package OLD;

import java.io.File;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

/**
 * 
 * @author Isais Sadaune
 *
 */
public class MathModule implements IModule {

	public String NAME = "Math";

	public MathModule(String hash) {

	}

	@Override
	public String handle(String[] args, MessageReceivedEvent event) {

		String message = "Veuillez saisir une commande valide.";
		int taille = args.length;
		try {
			if (args[0].equals("rand")) {

				int min = 0;
				int max = 100;

				if (taille == 2) {
					try {
						max = Integer.parseInt(args[1]); // verifier le type d'args[1]
						double i = Math.random() * (max - min);
						message = Integer.toString((int) i);
					} catch (Exception e) {
						if (params.containsKey("mathType")) {
							return params.get("mathType");
						} else {
							return Application.getMessage("mathType", Application.servers.get(event.getGuild().getId()));
						}
					}
				} else if (taille == 1) {
					double i = Math.random() * (max - min);
					message = Integer.toString((int) i);
				} else if (taille > 0) {
					if (params.containsKey("mathParam")) {
						return params.get("mathParam");
					} else {
						return Application.getMessage("mathParam", Application.servers.get(event.getGuild().getId()));
					}
				}
			}

			if (args[0].equals("cos")) {
				if (taille == 2) // verifier le type
				{
					try {
						double y = Math.toRadians(Double.parseDouble(args[1]));
						double x = Math.cos(y); // � mes souhaits
						message = String.valueOf(x);
					} catch (Exception e) {
						if (params.containsKey("mathType")) {
							return params.get("mathType");
						} else {
							return Application.getMessage("mathType", Application.servers.get(event.getGuild().getId()));
						}
					}
				} else if (params.containsKey("mathParam")) {
					return params.get("mathParam");
				} else {
					return Application.getMessage("mathParam", Application.servers.get(event.getGuild().getId()));
				}
			}

			if (args[0].equals("sin")) {
				if (taille == 2) // verifier le type
				{
					try {
						double y = Math.toRadians(Double.parseDouble(args[1]));
						double x = Math.sin(y); // � mes souhaits
						message = String.valueOf(x);
					} catch (Exception e) {
						if (params.containsKey("mathType")) {
							return params.get("mathType");
						} else {
							return Application.getMessage("mathType", Application.servers.get(event.getGuild().getId()));
						}
					}
				} else if (params.containsKey("mathParam")) {
					return params.get("mathParam");
				} else {
					return Application.getMessage("mathParam", Application.servers.get(event.getGuild().getId()));
				}
			}

			if (args[0].equals("tan")) {
				if (taille == 2) // verifier le type
				{
					try {
						double y = Math.toRadians(Double.parseDouble(args[1]));
						double x = Math.tan(y); // � mes souhaits
						message = String.valueOf(x);
					} catch (Exception e) {
						if (params.containsKey("mathType")) {
							return params.get("mathType");
						} else {
							return Application.getMessage("mathType", Application.servers.get(event.getGuild().getId()));
						}
					}
				} else if (params.containsKey("mathParam")) {
					return params.get("mathParam");
				} else {
					return Application.getMessage("mathParam", Application.servers.get(event.getGuild().getId()));
				}
			}

			if (args[0].contentEquals("pi")) {
				if (taille == 1) {
					message = String.valueOf(Math.PI);// verifier l'arrondi
				} else if (params.containsKey("mathParam")) {
					return params.get("mathParam");
				} else {
					return Application.getMessage("mathParam", Application.servers.get(event.getGuild().getId()));
				}
			}

			if (args[0].contentEquals("degres")) {
				if (taille == 2) {
					try {
						double x = Double.parseDouble(args[1]);
						x = Math.toDegrees(x);
						message = Double.toString(x);
					} catch (Exception e) {
						if (params.containsKey("mathType")) {
							return params.get("mathType");
						} else {
							return Application.getMessage("mathType", Application.servers.get(event.getGuild().getId()));
						}

					}
				} else {
					if (params.containsKey("mathParam")) {
						return params.get("mathParam");
					} else {
						return Application.getMessage("mathParam", Application.servers.get(event.getGuild().getId()));
					}
				}
			}
			if (args[0].contentEquals("radians")) {
				if (taille == 2) {
					try {
						double x = Double.parseDouble(args[1]);
						x = Math.toRadians(x);
						message = Double.toString(x);
					} catch (Exception e) {
						if (params.containsKey("mathType")) {
							return params.get("mathType");
						} else {
							return Application.getMessage("mathType", Application.servers.get(event.getGuild().getId()));
						}

					}
				} else {
					if (params.containsKey("mathParam")) {
						return params.get("mathParam");
					} else {
						return Application.getMessage("mathParam", Application.servers.get(event.getGuild().getId()));
					}
				}
			}

			return message;
		} catch (Exception e) {

			if (params.containsKey("mathEmpty")) {
				return params.get("mathEmpty");
			} else {
				return Application.getMessage("mathEmpty", Application.servers.get(event.getGuild().getId()));
			}
		}
	}

	@Override
	public String getHelp(Server s) {

		return "This method can return :\n"
				+ "- rand : A random number\n"
				+ "- cos: The cosinus of a number\n"
				+ "- sin: The sinus of a number\n"
				+ "- tan: The tangent of a number\n"
				+ "- pi: The number PI\n"
				+ "- degres: Conversion to Degres\n"
				+ "- radians: Conversion to Radians\n";
		
	}

	@Override
	public Message getAnswer(Message messageObj, String request) {

		messageObj.title = "Module " + this.NAME;

		return messageObj;
	}

	@Override
	public boolean hasFile() {

		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public File getFile() {

		// TODO Auto-generated method stub
		return null;
	}

}

package Audio;

import Application.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

public class MusicController {

	public final static int defaultVolume = 10;

	public static String addTrack(SlashCommandInteractionEvent event) {

		String url = event.getOption("url").getAsString();
		Application.musicManager.loadTrack(event.getGuild(), url);
		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setFrameBufferDuration(5);
		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setVolume(defaultVolume);

		Logs.info("Adding Track to AudioPlayer");

		return "$$audio.addTrack$$" + url;
	}

	public static String nextTrack(SlashCommandInteractionEvent event) {

		if (Application.musicManager.getPlayer(event.getGuild()).getListenner().getTrack().size() == 0) {
			Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().stopTrack();
			return "$$err.audio.noMoreTrack$$";
		}
		Application.musicManager.getPlayer(event.getGuild()).skipTrack();
		AudioTrack track = Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getPlayingTrack();

		return "$$audio.nextTrack$$" + track.getInfo().title;

	}

	public static String clear(SlashCommandInteractionEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().stopTrack();
		Application.musicManager.getPlayer(event.getGuild()).getListenner().getTrack().clear();
		Application.musicManager.getPlayer(event.getGuild()).skipTrack();
		return "$$audio.clearTrack$$";
	}

	public static String stop(SlashCommandInteractionEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setPaused(true);
		return "$$audio.pause$$";
	}

	public static String play(SlashCommandInteractionEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setPaused(false);
		return "$$audio.unpause$$";
	}

	public static String increaseVolume(SlashCommandInteractionEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer()
				.setVolume(Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getVolume() + defaultVolume);
		return "$$audio.increaseVolume$$";
	}

	public static String decreaseVolume(SlashCommandInteractionEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer()
				.setVolume(Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getVolume() - defaultVolume);
		return "$$audio.decreaseVolume$$";
	}

	public static String mute(SlashCommandInteractionEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer()
				.setVolume(0);

		return "$$audio.mute$$";
	}

	public static String unmute(SlashCommandInteractionEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer()
				.setVolume(defaultVolume);

		return "$$audio.unmute$$";
	}

	public static String info(SlashCommandInteractionEvent event) {

		String message = "";

		message += "$$audio.info.title$$\n";
		message += "$$audio.info.pause$$ "
				+ !Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().isPaused() + "\n";
		message += "$$audio.info.volume$$"
				+ (Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getVolume() * 10) + "%\n";

		message += "\n ** $$audio.info.playingTitle$$ ** \n\n";

		AudioTrack track = Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getPlayingTrack();

		message += "$$audio.info.trackTitle$$" + track.getInfo().title
				+ "\n";
		message += "$$audio.info.trackAuthor$$"
				+ track.getInfo().author + "\n";

		Date date = new Date(track.getDuration());
		Date datePos = new Date(track.getPosition());
		SimpleDateFormat dt = new SimpleDateFormat("mm:ss");
		if (track.getDuration() > 3600000) {
			dt = new SimpleDateFormat("HH:mm:ss");
			date.setHours(date.getHours() - 1);
			datePos.setHours(datePos.getHours() - 1);
		}
		message += "$$audio.info.trackTime$$ ["
				+ dt.format(datePos) + "/" + dt.format(date) + "]\n";
		message += "$$audio.info.trackURL$$" + track.getInfo().uri;

		message += "\n ** $$audio.info.nextTrack$$ **\n"
				+ list(event);
		return message;
	}

	public static String list(SlashCommandInteractionEvent event) {

		String message = "";
		BlockingQueue<AudioTrack> bq = Application.musicManager.getPlayer(event.getGuild()).getListenner().getTrack();
		int i = 0;
		for (AudioTrack at : bq) {
			i++;
			if (i <= 10) {
				message += i + " - " + at.getInfo().title + "\n";
			}
		}
		if (i > 10) {
			message += "..." + (i - 10) + "$$audio.addTrack$$";
		}

		return message;
	}

}

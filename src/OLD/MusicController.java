package OLD;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class MusicController {

	public static String addTrack(MessageReceivedEvent event) {

		String url = event.getMessage().getContentDisplay().split(" ")[1];
		Application.musicManager.loadTrack(event, url);
		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setFrameBufferDuration(5);
		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setVolume(10);
		return "Adding Track";
	}

	public static String nextTrack(MessageReceivedEvent event) {
		
		if(Application.musicManager.getPlayer(event.getGuild()).getListenner().getTrack().size() == 0) {
			Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().stopTrack();
		}
		Application.musicManager.getPlayer(event.getGuild()).skipTrack();
		if(Application.musicManager.getPlayer(event.getGuild()).getListenner().getTrack().size() == 0) {
			return "No more track";
		}
		AudioTrack track = Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getPlayingTrack();

		return "Next Track " + track.getInfo().title;

	}

	public static String clear(MessageReceivedEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().stopTrack();
		Application.musicManager.getPlayer(event.getGuild()).getListenner().getTrack().clear();
		Application.musicManager.getPlayer(event.getGuild()).skipTrack();
		return "PlayList clear";
	}

	public static String list(MessageReceivedEvent event) {

		String message = "";
		BlockingQueue<AudioTrack> bq = Application.musicManager.getPlayer(event.getGuild()).getListenner().getTrack();
		int i = 0;
		message = "Actual music : \n " + i + " - "
				+ Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getPlayingTrack().getInfo().title + "\n";
		message = "Comming music: \n";
		for (AudioTrack at : bq) {
			i++;
			if (i <= 10) {
				message += i + " - " + at.getInfo().title + "\n";
			}
		}
		if (i > 10) {
			message += "..." + (i - 10) + " other musics";
		}
		event.getTextChannel().sendMessage(message).queue();
		return message;
	}

	public static String stop(MessageReceivedEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setPaused(true);
		return "Music paused";
	}

	public static String play(MessageReceivedEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setPaused(false);
		return "Music played";
	}

	public static String getVolume(MessageReceivedEvent event) {
		return "Volume actuel : " + Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getVolume();
	}
	
	public static String increaseVolume(MessageReceivedEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer()
				.setVolume(Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getVolume() + 5);
		return "Sound increase";
	}

	public static String decreaseVolume(MessageReceivedEvent event) {

		Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer()
				.setVolume(Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getVolume() - 5);
		return "Sound decrease";
	}

	public static String setVolume(MessageReceivedEvent event, String volume) {
		
		try {
			Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setVolume(Integer.parseInt(volume));
			return "Sound volume set to " + volume;
		} catch (Exception e) {
			
			return "Volume must be a Integer";
		}
	}

	public static String getInfo(MessageReceivedEvent event) {

		String message = "";
		AudioTrack track = Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().getPlayingTrack();
		if (track.getInfo().isStream) {
			message += "[STREAM]\n";
		}
		message += "Titre : " + track.getInfo().title + "\n";
		message += "Author : " + track.getInfo().author + "\n";
		Date date = new Date(track.getDuration());
		Date datePos = new Date(track.getPosition());
		SimpleDateFormat dt = new SimpleDateFormat("mm:ss");
		if (track.getDuration() > 3600000) {
			dt = new SimpleDateFormat("HH:mm:ss");
			date.setHours(date.getHours() - 1);
			datePos.setHours(datePos.getHours() - 1);
		}
		message += "Temps : [" + dt.format(datePos) + "/" + dt.format(date) + "]\n";
		message += "URL : " + track.getInfo().uri;
		message += "{{https://i.ytimg.com/vi/" + track.getIdentifier() + "/mqdefault.jpg}}";
		return message;
	}

	public static String search(MessageReceivedEvent event, String[] args) {

		String message;
		try {
			String url = ApiCall.searchOnYoutube(args);
			Application.musicManager.loadTrack(event, "https://youtu.be/" + url);
			Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setFrameBufferDuration(5);
			Application.musicManager.getPlayer(event.getGuild()).getAudioPlayer().setVolume(10);
			Thread.sleep(2000);
			message = "Adding Track\n";
			message += getInfo(event);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "Nothing found";
		}
		return message;
	}
}
